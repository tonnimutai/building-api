// Methods
// { }

var bootcamp = {
  title: 'Building APIs in Nodejs',
  getTrainer: function getTrainer() {
    return 'Tony';
  }
};

console.log(bootcamp.title + ' with ' + bootcamp.getTrainer());