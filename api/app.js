var express = require('express')
var bodyParser = require('body-parser');
var app = express()

var DB = {
  users: []
};

app.use(bodyParser.json()); // for parsing application/json
 
 // GET Users
app.get('/users', function (req, res) {
  res.json(DB.users);
});

// CREATE Users
app.post('/users', function (req, res){
  // how do i get the request payload
  // how do i insert the payload to the db
  // what do i return to the requester
  
  var body = req.body;
  
  DB.users.push(body);
  
  res.json(body);
  
});
 
app.listen(3000)