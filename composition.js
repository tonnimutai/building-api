// Composition of interfaces.
// View and Get total number of trainees
// in real-time
// Timers: intervals of 100ms

var trainees = 4;
var bootcamp = 'Building APIs in NodeJS';

function getTotalTrainees () {
  var total = registerTrainee(2);
  
  return total;
}

function registerTrainee (num) {
  console.log(bootcamp + ' has a total of ' + trainees + ' trainees.');
  
  function worker(){
      // logic
      trainees = trainees + num;
  
      console.log(bootcamp + ' has a total of ' + trainees + ' trainees.');
  }
  
  setInterval(worker, 2000);
}

getTotalTrainees();






